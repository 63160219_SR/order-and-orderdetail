/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import com.mycompany.dao.OrdersDao;
import com.mycompany.model.OrderDetail;
import com.mycompany.model.Orders;
import com.mycompany.model.Product;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author sairung
 */
public class testOrder {

    public static void main(String[] args) {
        Product product1 = new Product(1, "A", 75);
        Product product2 = new Product(2, "B", 170);
        Product product3 = new Product(3, "C", 70);
        Orders order = new Orders();
        OrderDetail orderDetail1 = new OrderDetail(product1, product1.getName(), product1.getPrice(), 1, order);
//        ArrayList<OrderDetail> orderDetail1s = order.getOrderDetail();
//        orderDetail1s.add(orderDetail1);
//        order.addOrderDetail(orderDetail1);
        order.addOrderDetaill(product1, 1);
        order.addOrderDetaill(product2, 1);
        order.addOrderDetaill(product3, 1);
        System.out.println(order);
        System.out.println(order.getOrderDetail());
        printReclept(order);

        OrdersDao orderDao = new OrdersDao();
//        Orders newOrders = orderDao.save(order);
//        System.out.println(newOrders);
        System.out.println(orderDao.get(4));

    }

    static void printReclept(Orders order) {
        System.out.println("Ordet " + order.getId());
        for (OrderDetail od : order.getOrderDetail()) {
            System.out.println("" + od.getProductName() + " " + od.getQty() + " " + od.getProductPrice() + " " + od.getTotal());
        }
        System.out.println("Total: " + order.getTotal() + "Qty: " + order.getQty());
    }
}
